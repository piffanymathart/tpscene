package TPSceneIO;

/**
 * View structure for read/write via GSON reflection.
 */
public class View extends SceneIO.View {

	/**
	 * The projection targeting method.
	 * Valid values include: "none", "all", "normal-dir", "inv-normal-dir"
	 */
	public String targetMethod;
}
