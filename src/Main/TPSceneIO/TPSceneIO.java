package TPSceneIO;

import PolyhedronIO.PolyhedronIO;
import Scene.IScene;
import SceneIO.Model;
import SceneIO.SceneIO;
import SceneIO.Screen;
import TPScene.ITPScene;
import TPScene.TPScene;
import TextIO.TextIO;
import Unfolding.UnfoldablePolyhedron;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Managing IO operation on TPScene.
 */
public class TPSceneIO {

	/**
	 * Reads the string content and the file directory into a targeted projection scene.
	 * @param s The string content.
	 * @param dir The file directory.
	 * @return The targeted projection scene.
	 * @throws IOException If file reading fails.
	 */
	public static ITPScene read(String s, Path dir) throws IOException {

		IScene scene = SceneIO.read(s, dir);

		return new TPScene(
			scene.getSurface(),
			scene.getModels(),
			scene.getFovY(),
			ViewIO.readTargetMethod(GsonSceneIO.read(s).view)
		);
	}

	/**
	 * Writes the targeted projection scene to a file in the file directory.
	 * @param scene The targeted projection scene.
	 * @param dir The file directory.
	 * @throws IOException If file writing fails.
	 */
	public static void write(ITPScene scene, Path dir) throws IOException {

		GsonScene gsonScene = new GsonScene();

		gsonScene.view = new View();
		gsonScene.view.fov = ViewIO.writeFovY(scene.getFovY());
		gsonScene.view.targetMethod = ViewIO.writeTargetMethod(scene.getTargetMethod());

		int n = scene.getModels().length;
		gsonScene.models = new Model[n];
		for(int i=0; i<n; i++) {
			gsonScene.models[i] = new Model();
			gsonScene.models[i].file = String.format("model%04d.obj", i);
		}

		gsonScene.screen = new Screen();
		gsonScene.screen.file = "screen.obj";

		String sceneStr = GsonSceneIO.write(gsonScene);
		TextIO.write(appendToPath(dir, "scene.json"), sceneStr);

		String screenStr = PolyhedronIO.write(scene.getSurface().getUnfoldablePolyhedron());
		TextIO.write(appendToPath(dir, gsonScene.screen.file), screenStr);

		for(int i=0; i<n; i++) {
			String modelStr = PolyhedronIO.write(new UnfoldablePolyhedron(scene.getModel(i), null));
			TextIO.write(appendToPath(dir, gsonScene.models[i].file), modelStr);
		}
	}

	/**
	 * Appends a file name to a directory path.
	 * @param dirPath The directory path.
	 * @param filename The file name.
	 * @return The full file path.
	 */
	private static Path appendToPath(Path dirPath, String filename) {
		return Paths.get(dirPath.toString() + "\\" + filename);
	}
}
