package TPSceneIO;

/**
 * GsonScene structure for read/write via Gson reflection.
 */
class GsonScene {

	/**
	 * Gson view structure.
	 */
	View view;

	/**
	 * Gson model structures.
	 */
	SceneIO.Model[] models;

	/**
	 * Gson screen structure.
	 */
	SceneIO.Screen screen;

	/**
	 * Constructs an empty scene.
	 */
	GsonScene() {}
}
