package TPSceneIO;

import TPScene.TPScene.TargetMethod;

import java.io.IOException;

import static TPScene.TPScene.TargetMethod.*;

/**
 * Reads in view parameters.
 */
public class ViewIO extends SceneIO.ViewIO {

	/**
	 * Reads the projection targeting method.
	 * @param view The View object.
	 * @return The projection targeting method.
	 * @throws IOException If the input is invalid.
	 */
	public static TargetMethod readTargetMethod(View view) throws IOException {

		String errMsg = "Invalid targetMethod; valid values include \"none\", \"all\", \"normal-dir\", and \"inv-normal-dir\"";

		if(view==null) throw new IOException(errMsg);

		if(view.targetMethod == null) return TARGET_ALL;

		switch(view.targetMethod) {
			case "none": return TARGET_NONE;
			case "all": return TARGET_ALL;
			case "normal-dir": return TARGET_BY_NORMAL;
			case "inv-normal-dir": return TARGET_BY_INVERSE_NORMAL;
			default: throw new IOException(errMsg);
		}
	}

	/**
	 * Writes the projection targeting method to a Gson View string value.
	 * @param targetMethod The projection target method.
	 * @return The corresponding Gson View string value.
	 */
	public static String writeTargetMethod(TargetMethod targetMethod) {

		switch(targetMethod) {
			case TARGET_NONE: return "none";
			case TARGET_BY_NORMAL: return "normal-dir";
			case TARGET_BY_INVERSE_NORMAL: return "inv-normal-dir";
			case TARGET_ALL: default: return "all";
		}
	}
}
