package TPScene;

import FlaggedProjection.ICell;
import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;
import Mesh.IMesh;
import Mesh.IMeshLine;
import Mesh.IMeshPolygon;
import Mesh.Mesh;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

class ProjectionTargetingManager {

	/**
	 * Splits a mesh into two meshes: one containing front-facing elements and one containing
	 * back-facing elements.
	 * @param mesh The mesh.
	 * @param tol The dot produce tolerance for determining front- and back-facing.
	 * @return Two meshes: one containing front-facing elements and one containing back-facing elements.
	 */
	IMesh[] splitIntoFrontAndBackFacingElements(IMesh mesh, double tol) {

		IMeshPolygon[] faces = mesh.getFaces();
		IMeshLine[] edges = mesh.getEdges();

		List<IMeshLine> frontEdges = new ArrayList<>();
		List<IMeshPolygon> frontFaces = new ArrayList<>();

		List<IMeshLine> backEdges = new ArrayList<>();
		List<IMeshPolygon> backFaces = new ArrayList<>();

		for(IMeshLine edge : edges) {
			List<Vector3d> normals = edge.getNormals();
			switch(normals.size()) {
				case 0: {
					frontEdges.add(edge);
					backEdges.add(edge);
					break;
				}
				case 1: {
					if(isFacing(edge, normals.get(0), true, tol)) frontEdges.add(edge);
					if(isFacing(edge, normals.get(0), false, tol)) backEdges.add(edge);
					break;
				}
				case 2: {
					Vector3d normal1 = normals.get(0);
					Vector3d normal2 = normals.get(1);

					if(isFacing(edge, normal1, true, tol) || isFacing(edge, normal2, true, tol)) {
						frontEdges.add(edge);
					}

					if(isFacing(edge, normal1, false, tol) || isFacing(edge, normal2, false, tol)) {
						backEdges.add(edge);
					}
					break;
				}
				default: throw new RuntimeException("Edge should not have more than two associated normal vectors.");
			}
		}

		for(IMeshPolygon face : faces) {
			Vector3d normal = face.getNormal();
			if(normal==null) {
				frontFaces.add(face);
				backFaces.add(face);
				continue;
			}
			if(isFacing(face, normal, true, tol)) {
				frontFaces.add(face);
			}
			if(isFacing(face, normal, false, tol)) {
				backFaces.add(face);
			}
		}

		IMesh frontModel = new Mesh(
			frontEdges.toArray(new IMeshLine[frontEdges.size()]),
			frontFaces.toArray(new IMeshPolygon[frontFaces.size()])
		);

		IMesh backModel = new Mesh(
			backEdges.toArray(new IMeshLine[backEdges.size()]),
			backFaces.toArray(new IMeshPolygon[backFaces.size()])
		);

		return new IMesh[] {frontModel, backModel};
	}

	/**
	 * Checks whether a cell is facing the specified direction (front or back).
	 * @param cell The cell.
	 * @param front Whether to test front-facing or back-facing.
	 * @param tol Tolerance for the dot product being 1 or -1.
	 * @return Whether the cell is facing the specified direction.
	 */
	public boolean isFacing(ICell cell, boolean front, double tol) {
		return isFacing(cell.getCentre(), cell.getPlane().getNormal(), front, tol);
	}

	/**
	 * Checks whether a point is facing the specified direction (front or back).
	 * @param p The point.
	 * @param front Whether to test front-facing or back-facing.
	 * @param tol Tolerance for the dot product being 1 or -1.
	 * @return Whether the point is facing the specified direction.
	 */
	private boolean isFacing(Point3d p, Vector3d normal, boolean front, double tol) {
		if(p == null) return false;
		Vector3d normNormal = new Vector3d(normal);
		normNormal.normalize();
		Vector3d pv = new Vector3d(p);
		pv.normalize();
		if(front) return pv.dot(normNormal) < tol;
		else return pv.dot(normNormal) > -tol;
	}

	/**
	 * Checks whether (part of) a line is facing the specified direction (front or back).
	 * @param line The line.
	 * @param front Whether to test front-facing or back-facing.
	 * @param tol Tolerance for the dot product being 1 or -1.
	 * @return Whether the line is facing the specified direction.
	 */
	private boolean isFacing(ILine3d line, Vector3d normal, boolean front, double tol) {
		if(line == null) return false;
		if(isFacing(line.getP1(), normal, front, tol)) return true;
		if(isFacing(line.getP2(), normal, front, tol)) return true;
		return false;
	}

	/**
	 * Checks whether (part of) a polygon is facing the specified direction (front or back).
	 * @param polygon The polygon.
	 * @param front Whether to test front-facing or back-facing.
	 * @param tol Tolerance for the dot product being 1 or -1.
	 * @return Whether the polygon is facing the specified direction.
	 */
	private boolean isFacing(IPolygon3d polygon, Vector3d normal, boolean front, double tol) {
		if(polygon == null) return false;
		for(Point3d p : polygon.getVertices()) {
			if(isFacing(p, normal, front, tol)) return true;
		}
		return false;
	}
}
