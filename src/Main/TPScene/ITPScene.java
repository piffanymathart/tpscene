package TPScene;

import Scene.IScene;
import TPScene.TPScene.TargetMethod;

public interface ITPScene extends IScene {

	/**
	 * Gets the projection type.
	 * @return The projection type.
	 */
	TargetMethod getTargetMethod();

	/**
	 * Sets the projection type.
	 * @param targetMethod The projection targeting method.
	 */
	void setTargetMethod(TargetMethod targetMethod);
}
