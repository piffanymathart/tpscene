package TPScene;

import FlaggedProjection.ICell;
import Mesh.IMesh;
import Mesh.Mesh;
import Polyhedron.IColouredPolyhedron;
import Scene.ISurface;
import Scene.MeshConverter;
import Scene.Scene;

import java.util.ArrayList;
import java.util.List;

import static TPScene.TPScene.TargetMethod.TARGET_ALL;

/**
 * A scene that uses targeted projection methods.
 */
public class TPScene extends Scene implements ITPScene {

	/**
	 * Manages the projection targeting.
	 */
	private ProjectionTargetingManager m_projectionTargetingManager = new ProjectionTargetingManager();

	/**
	 * The projection targeting method.
	 */
	private TargetMethod m_targetMethod;

	/**
	 * The types of projection targeting methods.
	 */
	public enum TargetMethod {
		TARGET_NONE, // projects onto none of the surfaces
		TARGET_ALL, // projects onto all the surfaces
		TARGET_BY_NORMAL, // projects onto only surfaces with matching normal vector orientations
		TARGET_BY_INVERSE_NORMAL // projects onto only surfaces with opposing normal vector orientations
	}

	/**
	 * Constructs a projection targeted scene.
	 * @param surface The projection surface.
	 * @param models The models to be projected.
	 * @param fovY The vertical field-of-view.
	 * @param targetMethod The projection targeting method.
	 */
	public TPScene(ISurface surface, IColouredPolyhedron[] models, double fovY, TargetMethod targetMethod) {

		init(surface, models, fovY);
		m_targetMethod = targetMethod;
		recomputeProjection();
	}

	/**
	 * Gets the projection type.
	 * @return The projection type.
	 */
	public TargetMethod getTargetMethod() {
		return m_targetMethod;
	}

	/**
	 * Sets the projection type.
	 * @param targetMethod The projection targeting method.
	 */
	public void setTargetMethod(TargetMethod targetMethod) {
		m_targetMethod = targetMethod;
	}

	/**
	 * Recomputes the projection based on the surface, the models, and the projection targeting method.
	 */
	@Override
	public void recomputeProjection() {

		if(m_targetMethod == TARGET_ALL) super.recomputeProjection();
		else {
			ICell[] cells = m_surface.getCells();
			for (int i = 0; i < cells.length; i++) {
				m_projections[i] = computeCellProjections(cells[i], m_models);
			}
		}
	}

	/**
	 * Computes the projections of a set of models on a single cell.
	 * @param cell The cell.
	 * @param models The models.
	 * @return The projections.
	 */
	private IMesh[] computeCellProjections(ICell cell, IColouredPolyhedron[] models) {
		switch (m_targetMethod) {
			case TARGET_BY_NORMAL:
				return computeCellProjectionsTypeNormal(cell, models);
			case TARGET_BY_INVERSE_NORMAL:
				return computeCellProjectionsTypeInverseNormal(cell, models);
			case TARGET_NONE: default:
				return new IMesh[0];
		}
	}

	/**
	 * A helper method for computing projections with normal vector matching targeting method.
	 * @param cell The cell on which to project.
	 * @param models The models to project.
	 * @return The projections.
	 */
	private IMesh[] computeCellProjectionsTypeNormal(ICell cell, IColouredPolyhedron[] models) {

		if (cell == null) return new IMesh[0];
		List<IMesh> projection = new ArrayList<>();
		for (IColouredPolyhedron model : models) {
			List<IMesh> cellModelProjections = new ArrayList<>();

			double tol = 0.0;
			IMesh modelMesh = MeshConverter.toMesh(model);
			IMesh[] frontBackModel = m_projectionTargetingManager.splitIntoFrontAndBackFacingElements(modelMesh, tol);

			boolean isFrontFacing, isBackFacing;
			try {
				isFrontFacing = m_projectionTargetingManager.isFacing(cell, true, tol);
				isBackFacing = m_projectionTargetingManager.isFacing(cell, false, tol);
			} catch (Exception e) {
				continue;
			}

			if (isFrontFacing) {
				cellModelProjections.add(FLAGGED_PROJECTION_MANAGER.projectAndClipToCell(frontBackModel[0], cell));
			}
			if (isBackFacing) {
				cellModelProjections.add(FLAGGED_PROJECTION_MANAGER.projectAndClipToCell(frontBackModel[1], cell));
			}

			IMesh combinedProjection = new Mesh(cellModelProjections.toArray(new IMesh[cellModelProjections.size()]));
			projection.add(combinedProjection);
		}
		return projection.toArray(new IMesh[projection.size()]);
	}

	/**
	 * A helper method for computing projections with inverse normal vector matching targeting method.
	 * @param cell The cell on which to project.
	 * @param models The models to project.
	 * @return The projections.
	 */
	private IMesh[] computeCellProjectionsTypeInverseNormal(ICell cell, IColouredPolyhedron[] models) {
		if (cell == null) return new IMesh[0];
		List<IMesh> projection = new ArrayList<>();
		for (IColouredPolyhedron model : models) {
			List<IMesh> cellModelProjections = new ArrayList<>();

			double tol = 0.0;
			IMesh modelMesh = MeshConverter.toMesh(model);
			IMesh[] frontBackModel = m_projectionTargetingManager.splitIntoFrontAndBackFacingElements(modelMesh, tol);

			boolean isFrontFacing, isBackFacing;
			try {
				isFrontFacing = m_projectionTargetingManager.isFacing(cell, true, tol);
				isBackFacing = m_projectionTargetingManager.isFacing(cell, false, tol);
			} catch (Exception e) {
				continue;
			}

			if (isFrontFacing) {
				cellModelProjections.add(FLAGGED_PROJECTION_MANAGER.projectAndClipToCell(frontBackModel[1], cell));
			}
			if (isBackFacing) {
				cellModelProjections.add(FLAGGED_PROJECTION_MANAGER.projectAndClipToCell(frontBackModel[0], cell));
			}

			IMesh combinedProjection = new Mesh(cellModelProjections.toArray(new IMesh[cellModelProjections.size()]));
			projection.add(combinedProjection);
		}
		return projection.toArray(new IMesh[projection.size()]);
	}
}
