package TPSceneIO;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static TPScene.TPScene.TargetMethod.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ViewIOTest {

	@Test
	void readFovY() throws IOException {

		assertThrows(IOException.class, () -> ViewIO.readFovY(null));

		assertThrows(IOException.class, () -> ViewIO.readFovY(new View()));

		View view = new View();
		view.fov = 45;
		assertEquals(Math.PI/4.0, ViewIO.readFovY(view), 1e-5);
	}

	@Test
	void readTargetMethod() throws IOException {

		assertThrows(IOException.class, () -> ViewIO.readTargetMethod(null));

		assertEquals(TARGET_ALL, ViewIO.readTargetMethod(new View()));

		View view = new View();

		view.targetMethod = "none";
		assertEquals(TARGET_NONE, ViewIO.readTargetMethod(view));

		view.targetMethod = "all";
		assertEquals(TARGET_ALL, ViewIO.readTargetMethod(view));

		view.targetMethod = "normal-dir";
		assertEquals(TARGET_BY_NORMAL, ViewIO.readTargetMethod(view));

		view.targetMethod = "inv-normal-dir";
		assertEquals(TARGET_BY_INVERSE_NORMAL, ViewIO.readTargetMethod(view));

		view.targetMethod = "";
		assertThrows(IOException.class, () -> ViewIO.readTargetMethod(view));

		view.targetMethod = "gibberish";
		assertThrows(IOException.class, () -> ViewIO.readTargetMethod(view));
	}

	@Test
	void writeFovY() throws IOException {

		assertEquals(45, ViewIO.writeFovY(Math.PI/4.0), 1e-5);
	}

	@Test
	void writeTargetMethod() {

		assertEquals("none", ViewIO.writeTargetMethod(TARGET_NONE));
		assertEquals("all", ViewIO.writeTargetMethod(TARGET_ALL));
		assertEquals("normal-dir", ViewIO.writeTargetMethod(TARGET_BY_NORMAL));
		assertEquals("inv-normal-dir", ViewIO.writeTargetMethod(TARGET_BY_INVERSE_NORMAL));
	}

	@Test
	void forCoverageSake() {
		new ViewIO();
	}
}