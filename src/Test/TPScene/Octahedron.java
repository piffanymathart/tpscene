package TPScene;

import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;

import javax.vecmath.Point3d;

class Octahedron {

	static IUnfoldablePolyhedron create() {

		Point3d left = new Point3d(-1,0,0);	  // 0 = left
		Point3d right = new Point3d( 1,0,0);  // 1 = right
		Point3d top = new Point3d(0, 1,0);    // 2 = top
		Point3d bottom = new Point3d(0,-1,0); // 3 = bottom
		Point3d front = new Point3d(0,0, 1);  // 4 = front
		Point3d back = new Point3d(0,0,-1);   // 5 = back

		Point3d[] verts = {left, right, top, bottom, front, back};

		int[][] edgeInds = {
			// top-left, top-front, top-right, top-back
			{2,0}, {2,4}, {2,1}, {2,5},
			// left-front, front-right, right-back, back-left
			{0,4}, {4,1}, {1,5}, {5,0},
			// left-bottom, front-bottom, right-bottom, back-bottom,
			{0,3}, {4,3}, {1,3}, {5, 3}
		};

		int[][] faceInds = {
			// upper-front-left, upper-front-right
			{0,4,2}, {4,1,2},
			// upper-back-right, upper-back-left
			{1,5,2}, {5,0,2},
			// lower-front-left, lower-front-right
			{0,3,4}, {4,3,1},
			// lower-back-right, lower-back-left
			{1,3,5}, {5,3,0}
		};

		return new UnfoldablePolyhedron(verts, edgeInds, faceInds, null, null);
	}

	static IUnfoldablePolyhedron createMissingOneFront() {

		Point3d left = new Point3d(-1,0,0);	  // 0 = left
		Point3d right = new Point3d( 1,0,0);  // 1 = right
		Point3d top = new Point3d(0, 1,0);    // 2 = top
		Point3d bottom = new Point3d(0,-1,0); // 3 = bottom
		Point3d front = new Point3d(0,0, 1);  // 4 = front
		Point3d back = new Point3d(0,0,-1);   // 5 = back

		Point3d[] verts = {left, right, top, bottom, front, back};

		int[][] edgeInds = {
			// top-left, top-front, top-right, top-back
			{2,0}, /*{2,4},*/ {2,1}, {2,5},
			// left-front, front-right, right-back, back-left
			{0,4}, {4,1}, {1,5}, {5,0},
			// left-bottom, front-bottom, right-bottom, back-bottom,
			{0,3}, {4,3}, {1,3}, {5, 3}
		};

		int[][] faceInds = {
			// upper-front-left, upper-front-right
			/*{0,4,2},*/ {4,1,2},
			// upper-back-right, upper-back-left
			{1,5,2}, {5,0,2},
			// lower-front-left, lower-front-right
			{0,3,4}, {4,3,1},
			// lower-back-right, lower-back-left
			{1,3,5}, {5,3,0}
		};

		return new UnfoldablePolyhedron(verts, edgeInds, faceInds, null, null);
	}

	static IUnfoldablePolyhedron createMissingOneBack() {

		Point3d left = new Point3d(-1,0,0);	  // 0 = left
		Point3d right = new Point3d( 1,0,0);  // 1 = right
		Point3d top = new Point3d(0, 1,0);    // 2 = top
		Point3d bottom = new Point3d(0,-1,0); // 3 = bottom
		Point3d front = new Point3d(0,0, 1);  // 4 = front
		Point3d back = new Point3d(0,0,-1);   // 5 = back

		Point3d[] verts = {left, right, top, bottom, front, back};

		int[][] edgeInds = {
			// top-left, top-front, top-right, top-back
			{2,0}, {2,4}, {2,1}, /*{2,5},*/
			// left-front, front-right, right-back, back-left
			{0,4}, {4,1}, {1,5}, {5,0},
			// left-bottom, front-bottom, right-bottom, back-bottom,
			{0,3}, {4,3}, {1,3}, {5, 3}
		};

		int[][] faceInds = {
			// upper-front-left, upper-front-right
			{0,4,2}, {4,1,2},
			// upper-back-right, upper-back-left
			/*{1,5,2},*/ {5,0,2},
			// lower-front-left, lower-front-right
			{0,3,4}, {4,3,1},
			// lower-back-right, lower-back-left
			{1,3,5}, {5,3,0}
		};

		return new UnfoldablePolyhedron(verts, edgeInds, faceInds, null, null);
	}
}
