package TPScene;

import Mesh.IMesh;
import Polyhedron.IColouredPolyhedron;
import Scene.ISurface;
import Scene.Surface;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static TPScene.TPScene.TargetMethod.*;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TPSceneTest {

	@Test
	void valuesConstructor_getters() {

		ISurface surface = new Surface(createTwoFaceSurface(500, -10));
		IColouredPolyhedron[] models = { Octahedron.create() };
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_ALL);

		assertEquals(surface, scene.getSurface());

		assertArrayEquals(
			scene.getModels(),
			new IColouredPolyhedron[] { Octahedron.create() }
		);
		assertEquals(scene.getModel(0), Octahedron.create());

		assertEquals(Math.PI/4.0, scene.getFovY());
		assertEquals(new Point3d(0,0,0), scene.getScreenFrontPos());
		assertEquals(new Vector3d(0,1,0), scene.getScreenUpVec());
	}

	@Test
	void getTargetMethod_setTargetMethod() {

		ISurface surface = new Surface(createTwoFaceSurface(500, -10));

		IColouredPolyhedron[] models = { Octahedron.create() };

		ITPScene scene = new TPScene(surface, models, Math.PI/4.0, TARGET_ALL);
		assertEquals(TARGET_ALL, scene.getTargetMethod());

		scene.setTargetMethod(TARGET_BY_NORMAL);
		assertEquals(TARGET_BY_NORMAL, scene.getTargetMethod());
	}

	@Test
	void recomputeProjection_targetNone() {

		ISurface surface = new Surface(createTwoFaceSurface(1000, -20));
		IColouredPolyhedron[] models = {
			Octahedron.createMissingOneFront(),
			Octahedron.createMissingOneBack()
		};

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));

		for(IColouredPolyhedron model : models) {
			model.transform(transMat);
		}
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_ALL);

		scene.setTargetMethod(TARGET_NONE);
		scene.recomputeProjection();

		IMesh[][] projections = scene.getProjections();
		assertEquals(2, projections.length); // 2 cells

		assertEquals(0, projections[0].length);
		assertEquals(0, projections[1].length);
	}

	@Test
	void recomputeProjection_targetAll() {

		ISurface surface = new Surface(createTwoFaceSurface(1000, -20));
		IColouredPolyhedron[] models = {
			Octahedron.createMissingOneFront(),
			Octahedron.createMissingOneBack()
		};

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));

		for(IColouredPolyhedron model : models) {
			model.transform(transMat);
		}
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_NONE);

		scene.setTargetMethod(TARGET_ALL);
		scene.recomputeProjection();

		IMesh[][] projections = scene.getProjections();
		assertEquals(2, projections.length); // 2 cells

		assertEquals(2, projections[0].length);
		assertEquals(2, projections[1].length);

		IMesh model1FrontProj = projections[0][0];
		IMesh model1BackProj = projections[0][1];
		IMesh model2FrontProj = projections[1][0];
		IMesh model2BackProj = projections[1][1];

		assertEquals(7, model1FrontProj.getFaces().length);
		assertEquals(7, model1BackProj.getFaces().length);
		assertEquals(7, model2FrontProj.getFaces().length);
		assertEquals(7, model2BackProj.getFaces().length);

		assertEquals(11, model1FrontProj.getEdges().length);
		assertEquals(11, model1BackProj.getEdges().length);
		assertEquals(11, model2FrontProj.getEdges().length);
		assertEquals(11, model2BackProj.getEdges().length);
	}

	@Test
	void recomputeProjection_targetNormal() {

		ISurface surface = new Surface(createTwoFaceSurface(1000, -20));
		IColouredPolyhedron[] models = {
			Octahedron.createMissingOneFront(),
			Octahedron.createMissingOneBack()
		};

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));

		for(IColouredPolyhedron model : models) {
			model.transform(transMat);
		}
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_NONE);

		scene.setTargetMethod(TARGET_BY_NORMAL);
		scene.recomputeProjection();

		IMesh[][] projections = scene.getProjections();
		assertEquals(2, projections.length); // 2 cells

		assertEquals(2, projections[0].length);
		assertEquals(2, projections[1].length);

		IMesh model1FrontProj = projections[0][0];
		IMesh model1BackProj = projections[0][1];
		IMesh model2FrontProj = projections[1][0];
		IMesh model2BackProj = projections[1][1];

		assertEquals(3, model1FrontProj.getFaces().length);
		assertEquals(4, model1BackProj.getFaces().length);
		assertEquals(4, model2FrontProj.getFaces().length);
		assertEquals(3, model2BackProj.getFaces().length);

		assertEquals(6, model1FrontProj.getEdges().length);
		assertEquals(8, model1BackProj.getEdges().length);
		assertEquals(8, model2FrontProj.getEdges().length);
		assertEquals(6, model2BackProj.getEdges().length);
	}

	@Test
	void recomputeProjection_targetInvNormal() {

		ISurface surface = new Surface(createTwoFaceSurface(1000, -20));
		IColouredPolyhedron[] models = {
			Octahedron.createMissingOneFront(),
			Octahedron.createMissingOneBack()
		};

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));

		for(IColouredPolyhedron model : models) {
			model.transform(transMat);
		}
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_NONE);

		scene.setTargetMethod(TARGET_BY_INVERSE_NORMAL);
		scene.recomputeProjection();

		IMesh[][] projections = scene.getProjections();
		assertEquals(2, projections.length); // 2 cells

		assertEquals(2, projections[0].length);
		assertEquals(2, projections[1].length);

		IMesh model1FrontProj = projections[0][0];
		IMesh model1BackProj = projections[0][1];
		IMesh model2FrontProj = projections[1][0];
		IMesh model2BackProj = projections[1][1];

		assertEquals(4, model1FrontProj.getFaces().length);
		assertEquals(3, model1BackProj.getFaces().length);
		assertEquals(3, model2FrontProj.getFaces().length);
		assertEquals(4, model2BackProj.getFaces().length);

		assertEquals(8, model1FrontProj.getEdges().length);
		assertEquals(6, model1BackProj.getEdges().length);
		assertEquals(6, model2FrontProj.getEdges().length);
		assertEquals(8, model2BackProj.getEdges().length);
	}

	@Test
	void recomputeProjection_targetNormal_notFacingFrontOrBack() {

		ISurface surface = new Surface(createDegenerateSurface(1000, -20));
		IColouredPolyhedron[] models = {
			Octahedron.createMissingOneFront(),
			Octahedron.createMissingOneBack()
		};

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));

		for(IColouredPolyhedron model : models) {
			model.transform(transMat);
		}
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_BY_NORMAL);

		IMesh[][] projections = scene.getProjections();
		assertEquals(1, projections.length); // 1 cell

		assertEquals(0, projections[0].length);
	}

	@Test
	void recomputeProjection_targetInvNormal_notFacingFrontOrBack() {

		ISurface surface = new Surface(createDegenerateSurface(1000, -20));
		IColouredPolyhedron[] models = {
			Octahedron.createMissingOneFront(),
			Octahedron.createMissingOneBack()
		};

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));

		for(IColouredPolyhedron model : models) {
			model.transform(transMat);
		}
		double fovY = Math.PI/4.0;

		ITPScene scene = new TPScene(surface, models, fovY, TARGET_BY_INVERSE_NORMAL);

		IMesh[][] projections = scene.getProjections();
		assertEquals(1, projections.length); // 1 cell

		assertEquals(0, projections[0].length);
	}

	private IUnfoldablePolyhedron createTwoFaceSurface(double w, double z) {

		double lowerY = -w/2.0*Math.sqrt(3)/3.0;
		double upperY = w*Math.sqrt(3)/3.0;
		Point3d[] verts = {
			new Point3d(-w/2, lowerY, z),
			new Point3d(w/2, lowerY, z),
			new Point3d(0, upperY, z)
		};
		int[][] edgeInds = {};
		int[][] faceInds = { {0,1,2}, {2,1,0} };

		return new UnfoldablePolyhedron(verts, edgeInds, faceInds, null, null);
	}

	private IUnfoldablePolyhedron createDegenerateSurface(double w, double z) {

		Point3d[] verts = {
			new Point3d(0, w/2, z),
			new Point3d(0, 0, z),
			new Point3d(0, -w/2, z)
		};
		int[][] edgeInds = {};
		int[][] faceInds = { {0,1,2} };

		return new UnfoldablePolyhedron(verts, edgeInds, faceInds, null, null);
	}
}