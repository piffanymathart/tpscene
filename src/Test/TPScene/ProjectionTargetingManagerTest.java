package TPScene;

import FlaggedProjection.Cell;
import FlaggedProjection.ICell;
import FlaggedProjection.IPlane;
import FlaggedProjection.Plane;
import Mesh.*;
import Scene.MeshConverter;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ProjectionTargetingManagerTest {

	private static ProjectionTargetingManager PROJ_TARGET_MANAGER = new ProjectionTargetingManager();

	@Test
	void splitIntoFrontAndBackFaces_cube() {

		IMesh mesh = MeshConverter.toMesh(Octahedron.create());
		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));
		mesh.transform(transMat);

		IMesh[] frontBackMeshes = PROJ_TARGET_MANAGER.splitIntoFrontAndBackFacingElements(mesh, 0);
		IMesh frontMesh = frontBackMeshes[0];
		IMesh backMesh = frontBackMeshes[1];

		IMeshPolygon[] faces = mesh.getFaces();
		IMeshLine[] edges = mesh.getEdges();

		assertArrayEquals(
			new IMeshPolygon[] {faces[0], faces[1], faces[4], faces[5]},
			frontMesh.getFaces()
		);

		assertArrayEquals(
			new IMeshPolygon[] {faces[2], faces[3], faces[6], faces[7]},
			backMesh.getFaces()
		);

		assertArrayEquals(
			new IMeshLine[] {edges[0], edges[1], edges[2], edges[4], edges[5], edges[8], edges[9], edges[10]},
			frontMesh.getEdges()
		);

		assertArrayEquals(
			new IMeshLine[] {edges[0], edges[2], edges[3], edges[6], edges[7], edges[8], edges[10], edges[11]},
			backMesh.getEdges()
		);
	}

	@Test
	void splitIntoFrontAndBackFaces_faceWithZeroNormals() {

		IMeshPolygon[] meshFaces = {
			new MeshPolygon(
				new Point3d[] {
					new Point3d(0,0,0),
					new Point3d(1,0,0),
					new Point3d(0,1,0)
				},
				null,
				false,
				null
			)
		};
		IMeshLine[] meshEdges = {};
		IMesh mesh = new Mesh(meshEdges, meshFaces);

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));
		mesh.transform(transMat);

		IMesh[] frontBackMeshes = PROJ_TARGET_MANAGER.splitIntoFrontAndBackFacingElements(mesh, 0);
		IMesh frontMesh = frontBackMeshes[0];
		IMesh backMesh = frontBackMeshes[1];

		IMeshPolygon[] faces = mesh.getFaces();
		assertArrayEquals(faces, frontMesh.getFaces());
		assertArrayEquals(faces, backMesh.getFaces());
	}

	@Test
	void splitIntoFrontAndBackFaces_edgeWithZeroNormals() {

		IMeshPolygon[] meshFaces = {};
		IMeshLine[] meshEdges = {
			new MeshLine(
				new Point3d(0,0,0),
				new Point3d(1,0,0),
				new ArrayList<>(),
				false
			)
		};
		IMesh mesh = new Mesh(meshEdges, meshFaces);

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));
		mesh.transform(transMat);

		IMesh[] frontBackMeshes = PROJ_TARGET_MANAGER.splitIntoFrontAndBackFacingElements(mesh, 0);
		IMesh frontMesh = frontBackMeshes[0];
		IMesh backMesh = frontBackMeshes[1];

		IMeshLine[] edges = mesh.getEdges();
		assertArrayEquals(edges, frontMesh.getEdges());
		assertArrayEquals(edges, backMesh.getEdges());
	}

	@Test
	void splitIntoFrontAndBackFaces_edgeWithOneNormal() {

		IMeshPolygon[] meshFaces = {};
		IMeshLine[] meshEdges = {
			new MeshLine(
				new Point3d(0,0,0),
				new Point3d(1,0,0),
				Arrays.asList(new Vector3d(0,0,1)),
				false
			)
		};
		IMesh mesh = new Mesh(meshEdges, meshFaces);

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));
		mesh.transform(transMat);

		IMesh[] frontBackMeshes = PROJ_TARGET_MANAGER.splitIntoFrontAndBackFacingElements(mesh, 0);
		IMesh frontMesh = frontBackMeshes[0];
		IMesh backMesh = frontBackMeshes[1];

		IMeshLine[] edges = mesh.getEdges();
		assertArrayEquals(edges, frontMesh.getEdges());
		assertEquals(0, backMesh.getEdges().length);
	}

	@Test
	void splitIntoFrontAndBackFaces_edgeWithThreeNormals() {

		IMeshPolygon[] meshFaces = {};
		IMeshLine[] meshEdges = {
			new MeshLine(
				new Point3d(0,0,0),
				new Point3d(1,0,0),
				Arrays.asList(
					new Vector3d(0,0,1),
					new Vector3d(0,1,1),
					new Vector3d(1,1,1)
				),
				false
			)
		};
		IMesh mesh = new Mesh(meshEdges, meshFaces);

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0,0,-10));
		mesh.transform(transMat);

		assertThrows(
			RuntimeException.class,
			() -> PROJ_TARGET_MANAGER.splitIntoFrontAndBackFacingElements(mesh, 0)
		);
	}

	@Test
	void isFacing1() {

		// triangle is in front of the observer with normal = (0,0,1)

		IPlane plane = new Plane(
			new Point3d(0,0,-3),
			new Point3d(1,0,-3),
			new Point3d(0,1,-3)
		);
		ICell cell = new Cell(plane);

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing2() {

		// triangle is behind the observer with normal = (0,0,1)

		IPlane plane = new Plane(
			new Point3d(0,0,3),
			new Point3d(1,0,3),
			new Point3d(0,1,3)
		);
		ICell cell = new Cell(plane);

		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing3() {

		// triangle is in front of the observer with normal = (0,0,-1)

		IPlane plane = new Plane(
			new Point3d(0,1,-3),
			new Point3d(1,0,-3),
			new Point3d(0,0,-3)
		);
		ICell cell = new Cell(plane);

		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing4() {

		// triangle is in front of the observer slightly tilted toward the observer

		IPlane plane = new Plane(
			new Point3d(0,0,-3),
			new Point3d(0.1,1,-5),
			new Point3d(0,2,-3)
		);
		ICell cell = new Cell(plane);

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing5() {

		// triangle is in front of the observer slightly tilted away from the observer

		IPlane plane = new Plane(
			new Point3d(0,0,-3),
			new Point3d(-0.1,1,-5),
			new Point3d(0,2,-3)
		);
		ICell cell = new Cell(plane);

		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing6() {

		// triangle is to the left with normal = (1,0,0)

		IPlane plane = new Plane(
			new Point3d(-10,0,-3),
			new Point3d(-10,1,-5),
			new Point3d(-10,2,-3)
		);
		ICell cell = new Cell(plane);

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing7() {

		// triangle is to the right with normal = (1,0,0)

		IPlane plane = new Plane(
			new Point3d(10,0,-3),
			new Point3d(10,1,-5),
			new Point3d(10,2,-3)
		);
		ICell cell = new Cell(plane);

		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing8() {

		// triangle is below with normal = (0,1,0)

		IPlane plane = new Plane(
			new Point3d(-1,-10,-3),
			new Point3d(1,-10,-3),
			new Point3d(0,-10,-5)
		);
		ICell cell = new Cell(plane);

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing9() {

		// triangle is above with normal = (0,1,0)

		IPlane plane = new Plane(
			new Point3d(-1,10,-3),
			new Point3d(1,10,-3),
			new Point3d(0,10,-5)
		);
		ICell cell = new Cell(plane);

		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));
	}

	@Test
	void isFacing10() {

		// triangle is in front (slightly to the left) with normal = (1,0,0)

		IPlane plane = new Plane(
			new Point3d(-0.1,-1,-3),
			new Point3d(-0.1,0,-5),
			new Point3d(-0.1,1,-3)
		);
		ICell cell = new Cell(plane);

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0));
		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, false, 0));

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0.01));
		assertFalse(PROJ_TARGET_MANAGER.isFacing(cell, false, 0.01));

		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, true, 0.1));
		assertTrue(PROJ_TARGET_MANAGER.isFacing(cell, false, 0.1));
	}
}