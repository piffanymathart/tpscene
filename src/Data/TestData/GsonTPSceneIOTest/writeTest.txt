{
  "view": {
    "targetMethod": "normal-dir",
    "fov": 40.0
  },
  "models": [
    {
      "file": "a.txt",
      "transformations": []
    },
    {
      "file": "b.txt",
      "transformations": []
    },
    {
      "file": "c.txt",
      "transformations": []
    }
  ],
  "screen": {
    "file": "d.txt",
    "transformations": []
  }
}